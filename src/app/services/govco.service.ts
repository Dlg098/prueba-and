import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GovcoService {

  url = 'https://navio-studio.firebaseio.com/govco';

  constructor( private http: HttpClient ) { }

  getListOtros() {
    return this.http.get(`${ this.url }/otros.json`)
                    .pipe(
                      map( resp => this.crearArreglo(resp))
                    );
  }
  getListInformate() {
    return this.http.get(`${ this.url }/informate.json`)
                    .pipe(
                      map( resp => this.crearArreglo(resp))
                    );
  }
  getListOpinion() {
    return this.http.get(`${ this.url }/opinion.json`)
                    .pipe(
                      map( resp => this.crearArreglo(resp))
                    );
  }
  getListProcedure() {
    return this.http.get(`${ this.url }/tramites.json`)
                    .pipe(
                      map( resp => this.crearArreglo(resp))
                    );
  }
  private crearArreglo( arregloObj: object){

    const coleccion: any[] = [];
    Object.keys( arregloObj).forEach(key => {
      const elemento: any =  arregloObj[key];
      coleccion.push(elemento);
    })

    if (arregloObj === null){ return []; }
    return coleccion;
  }
}

//--Se importanan los servicios correspondientes a map y HttpClient para la toma de los datos de db json se genera el consumo de datos mediante un *ngFor//