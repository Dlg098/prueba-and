import { TestBed } from '@angular/core/testing';

import { GovcoService } from './govco.service';

describe('GovcoService', () => {
  let service: GovcoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GovcoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
