import { Component, OnInit } from '@angular/core';
import { GovcoService } from '../../services/govco.service';

@Component({
  selector: 'app-procedure',
  templateUrl: './procedure.component.html',
  styleUrls: ['./procedure.component.css']
})
export class ProcedureComponent implements OnInit {

  public listProcedure: any[] = [];

  constructor( private govcoServices: GovcoService) { }

  ngOnInit(): void {
    this.govcoServices.getListProcedure().subscribe( resp => {
      console.log(resp);
      this.listProcedure = resp;
    });
  }

}