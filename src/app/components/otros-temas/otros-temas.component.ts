import { Component, OnInit } from '@angular/core';
import { GovcoService } from '../../services/govco.service';

@Component({
  selector: 'app-otros-temas',
  templateUrl: './otros-temas.component.html',
  styleUrls: ['./otros-temas.component.css']
})
export class OtrosTemasComponent implements OnInit {

  public listOtros: any[] = [];

  constructor( private govcoServices: GovcoService) { }
  ngOnInit(): void {
    this.govcoServices.getListOtros().subscribe( resp => {
      console.log(resp);
      this.listOtros = resp;
    });
}
}