import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtrosTemasComponent } from './otros-temas.component';

describe('OtrosTemasComponent', () => {
  let component: OtrosTemasComponent;
  let fixture: ComponentFixture<OtrosTemasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtrosTemasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtrosTemasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
