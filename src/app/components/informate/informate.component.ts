import { Component, OnInit } from '@angular/core';
import { GovcoService } from '../../services/govco.service';

@Component({
  selector: 'app-informate',
  templateUrl: './informate.component.html',
  styleUrls: ['./informate.component.css']
})
export class InformateComponent implements OnInit {

  public listInformate: any[] = [];

  constructor( private govcoServices: GovcoService) { }

  ngOnInit(): void {
    this.govcoServices.getListInformate().subscribe( resp => {
      console.log(resp);
      this.listInformate = resp;
    });
  }

}
