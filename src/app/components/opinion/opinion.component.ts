import { Component, OnInit } from '@angular/core';
import { GovcoService } from '../../services/govco.service';

@Component({
  selector: 'app-opinion',
  templateUrl: './opinion.component.html',
  styleUrls: ['./opinion.component.css']
})
export class OpinionComponent implements OnInit {

  public listOpinion: any[] = [];

  constructor( private govcoServices: GovcoService) { }

  ngOnInit(): void {
    this.govcoServices.getListOpinion().subscribe( resp => {
      console.log(resp);
      this.listOpinion = resp;
    });
  }

}
