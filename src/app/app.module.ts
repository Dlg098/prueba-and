import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { InformateComponent } from './components/informate/informate.component';
import { OtrosTemasComponent } from './components/otros-temas/otros-temas.component';
import { BannerComponent } from './components/banner/banner.component';
import { OpinionComponent } from './components/opinion/opinion.component';
import { WrapComponent } from './wrap/wrap.component';
import { ProcedureComponent } from './components/procedure/procedure.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    InformateComponent,
    OtrosTemasComponent,
    BannerComponent,
    OpinionComponent,
    WrapComponent,
    ProcedureComponent,
   ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
